﻿using System.Collections.Generic;
using System.Data.Entity;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Contracts.Repository
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAll();
        Category GetByName(string name);
        IEnumerable<Category> GetByCount(int count);
        DbSet<Category> Categories { get; set; }
    }
}