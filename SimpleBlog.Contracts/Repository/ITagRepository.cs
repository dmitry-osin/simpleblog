﻿using System.Collections.Generic;
using System.Data.Entity;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Contracts.Repository
{
    public interface ITagRepository
    {
        IEnumerable<Tag> GetAll();
        IEnumerable<Tag> FindByName(string name);
        IEnumerable<Tag> GetByCount(int count);
        DbSet<Tag> Tags { get; set; }
    }
}