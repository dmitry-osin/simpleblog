﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Contracts.Repository
{
    public interface IPostRepository
    {
        IEnumerable<Post> GetAll();
        IEnumerable<Post> GetByPublishFlag(bool isPublished);
        IEnumerable<Post> GetByCount(int count);
        IEnumerable<Post> GetByDateTime(DateTime dateTime);
        IEnumerable<Post> GetByTagName(Tag tag);
        IEnumerable<Post> GetByCategoryName(Category category);
        DbSet<Post> Posts { get; set; }
    }
}