﻿using System.Collections.Generic;

namespace SimpleBlog.Contracts.Repository
{
    public interface IRepository<TEntity> where TEntity : class 
    {
        TEntity Add(TEntity items);
        TEntity Attach(TEntity items);
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> items);
        void Update(TEntity items);
        void Remove(TEntity items);
        void RemoveRange(IEnumerable<TEntity> items);
        int Commit();
    }
}