﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleBlog.DomainModel
{
    public class Post
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public ICollection<Category> Categories { get; set; }
        public DateTime PublishDateTime { get; set; }
        public bool IsPublished { get; set; }
        public string UrlSlug { get; set; }
        public ApplicationUser Author { get; set; }

        public Post()
        {
            Tags = new List<Tag>();
            Categories = new List<Category>();
            PublishDateTime = DateTime.Now;
        }
    }
}
