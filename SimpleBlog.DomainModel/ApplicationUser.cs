﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SimpleBlog.DomainModel
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Posts = new List<Post>();
        }

        public ApplicationUser(string userName) : base(userName)
        {
            Posts = new List<Post>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie).ConfigureAwait(false);

            userIdentity.AddClaim(new Claim("UserName", UserName));
            userIdentity.AddClaim(new Claim("FullName", FullName));

            return userIdentity;
        }
        public string FullName { get; set; }
        public string NewPassword { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}