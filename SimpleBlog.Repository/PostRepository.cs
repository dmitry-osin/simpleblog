﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleBlog.Contracts.Repository;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Repository
{
    public class PostRepository : BaseRepository<Post, ApplicationUser>, IPostRepository
    {
        public PostRepository() : base("DefaultConnection")
        {
        }

        public PostRepository(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public IEnumerable<Post> GetAll()
        {
            return GetAsNoTrackingQueryable()
                .Include(x => x.Tags)
                .Include(x => x.Categories)
                .Include(x => x.Author)
                .ToArray();
        }

        public IEnumerable<Post> GetByPublishFlag(bool isPublished)
        {
            return GetAsNoTrackingQueryable()
                .Where(p => p.IsPublished == isPublished)
                .ToArray();
        }

        public IEnumerable<Post> GetByCount(int count)
        {
            return GetAsNoTrackingQueryable()
                .Take(count)
                .ToArray();
        }

        public IEnumerable<Post> GetByDateTime(DateTime dateTime)
        {
            return GetAsNoTrackingQueryable()
                .Where(p => p.PublishDateTime == dateTime)
                .ToArray();
        }

        public IEnumerable<Post> GetByTagName(Tag tag)
        {
            if (tag == null) throw new ArgumentNullException(nameof(tag));
            return GetAsNoTrackingQueryable()
                .Where(p => p.Tags.Contains(tag))
                .ToArray();
        }

        public IEnumerable<Post> GetByCategoryName(Category category)
        {
            if (category == null) throw new ArgumentNullException(nameof(category));
            return GetAsNoTrackingQueryable()
                .Where(p => p.Categories.Contains(category))
                .ToArray();
        }

        #region DbSet

        public DbSet<Post> Posts { get; set; }

        #endregion

        #region OnModelCreating

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Mappings(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private static void Mappings(DbModelBuilder modelBuilder)
        {
            modelBuilder.RegisterEntityType(typeof(Post));
            modelBuilder.Entity<Post>()
                .ToTable("Posts");

            modelBuilder.Entity<Post>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Post>()
                .Property(x => x.Content)
                .IsRequired();

            modelBuilder.Entity<Post>()
                .Property(x => x.Title)
                .HasMaxLength(200)
                .IsRequired();

            modelBuilder.Entity<Post>()
                .Property(x => x.Description)
                .HasMaxLength(300)
                .IsRequired();

            modelBuilder.Entity<Post>()
                .Property(x => x.UrlSlug)
                .IsRequired();

            modelBuilder.Entity<Post>()
                .Property(x => x.IsPublished)
                .IsOptional();

            modelBuilder.Entity<Post>()
                .Property(x => x.PublishDateTime)
                .IsRequired();

            modelBuilder.Entity<Post>()
                .HasRequired(x => x.Author)
                .WithMany(x => x.Posts);

            modelBuilder.Entity<Post>()
                .HasMany(x => x.Tags)
                .WithMany(x => x.Posts)
                .Map(m =>
                {
                    m.MapLeftKey("TagId");
                    m.MapRightKey("PostId");
                    m.ToTable("Tags_Posts");
                });

            modelBuilder.Entity<Post>()
                .HasMany(x => x.Categories)
                .WithMany(x => x.Posts)
                .Map(m =>
                {
                    m.MapLeftKey("CategoryId");
                    m.MapRightKey("PostId");
                    m.ToTable("Categories_Posts");
                });
        }

        #endregion
    }
}