﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Repository.Utils
{
    public static class DbInitializer
    {
        // DropCreateDatabaseAlways<DataContext>
        // DropCreateDatabaseIfModelChanges<DataContext>
        // MigrateDatabaseToLatestVersion<DataContext, Configuration>
        // CreateDatabaseIfNotExists<DataContext>

        static DbInitializer()
        {
            GenerateData();
        }

        public static List<Category> Categories { get; set; }
        public static List<Post> Posts { get; set; }
        public static List<Tag> Tags { get; set; }
        public static ApplicationUser User { get; set; }

        public static void GenerateData()
        {
            Tags = new List<Tag>();
            Posts = new List<Post>();
            Categories = new List<Category>();
            User = new ApplicationUser()
            {
                Email = "admin@mail.me",
                EmailConfirmed = true,
                UserName = "admin",
                FullName = "Ivan Petrov",
                PasswordHash = Guid.NewGuid().ToString()
            };

            for (int i = 0; i < 10; i++)
            {
                Tags.Add(new Tag()
                {
                    Name = $"Test{i}",
                    Description = $"Simple test tag{i}",
                    UrlSlug = $"test-{i}"
                });
            }

            for (int i = 0; i < 10; i++)
            {
                Categories.Add(new Category()
                {
                    Name = $"Category{i}",
                    Description = $"Simple test category{i}",
                    UrlSlug = $"test-category-{i}"
                });
            }

            for (int i = 0; i < 10; i++)
            {
                Posts.Add(new Post()
                {
                    Tags = Tags,
                    Categories = Categories,
                    UrlSlug = $"simple-test-url-{i}",
                    Description = $"Yet another post with number{i}",
                    Content = $"Simple content of post with number{i}",
                    Title = $"Simple post {i}",
                    IsPublished = true,
                    PublishDateTime = DateTime.UtcNow,
                    Author = User
                });
            }
        }


        public class TagsDbInitializer : CreateDatabaseIfNotExists<TagRepository>
        {
            protected override void Seed(TagRepository context)
            {
                context.AddRange(Tags);
                base.Seed(context);
            }
        }

        public class CategoriesDbInitializer : CreateDatabaseIfNotExists<CategoryRepository>
        {
            protected override void Seed(CategoryRepository context)
            {
                context.Categories.AddRange(Categories);
                base.Seed(context);
            }
        }

        public class PostsDbInitializer : CreateDatabaseIfNotExists<PostRepository>
        {
            protected override void Seed(PostRepository context)
            {
                context.Posts.AddRange(Posts);
                base.Seed(context);
            }
        }
    }
}