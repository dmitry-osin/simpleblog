﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleBlog.Contracts.Repository;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Repository
{
    public class TagRepository : BaseRepository<Tag, ApplicationUser>, ITagRepository
    {
        public TagRepository(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public TagRepository() : base("DefaultConnection")
        {
        }

        public IEnumerable<Tag> GetAll()
        {
            return GetAsNoTrackingQueryable().ToArray();
        }

        public IEnumerable<Tag> FindByName(string name)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            return GetAsNoTrackingQueryable()
                .Where(t => t.Name == name)
                .ToArray();
        }

        public IEnumerable<Tag> GetByCount(int count)
        {
            return GetAsNoTrackingQueryable()
                .Take(count)
                .ToArray();
        }

        #region DbSet

        public DbSet<Tag> Tags { get; set; }

        #endregion

        #region OnModelCreating

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Mappings(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private static void Mappings(DbModelBuilder modelBuilder)
        {
            modelBuilder.RegisterEntityType(typeof(Tag));

            modelBuilder.Entity<Tag>().ToTable("Tags");
            modelBuilder.Entity<Tag>().HasKey(x => x.Id);

            modelBuilder.Entity<Tag>()
                .Property(x => x.Name)
                .HasMaxLength(10)
                .IsRequired();

            modelBuilder.Entity<Tag>()
                .Property(x => x.Description)
                .HasMaxLength(150)
                .IsOptional();

            modelBuilder.Entity<Tag>()
                .Property(x => x.UrlSlug)
                .IsRequired();
        }

        #endregion
    }
}