﻿using System.Data.Entity.ModelConfiguration.Configuration;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleBlog.Contracts.Repository;
using SimpleBlog.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace SimpleBlog.Repository
{
    public abstract class BaseRepository<TEntity, TUser> : IdentityDbContext<TUser>, IRepository<TEntity> where TUser : IdentityUser where TEntity : class
    {
        protected BaseRepository(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public TEntity Add(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return Set<TEntity>().Add(entity);
        }

        public TEntity Attach(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return Set<TEntity>().Attach(entity);
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new ArgumentNullException(nameof(entities));
            return Set<TEntity>().AddRange(entities);
        }

        public void Update(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Set<TEntity>().AddOrUpdate(entity);
        }

        public void Remove(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new ArgumentNullException(nameof(entities));
            Set<TEntity>().RemoveRange(entities);
        }

        public TEntity Find(int id)
        {
            return Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> expression)
        {
            return GetAsNoTrackingQueryable().Where(expression).ToArray();
        }

        public TEntity FindByKey(int id)
        {
            var item = Expression.Parameter(typeof(TEntity), "entity");
            var prop = Expression.Property(item, typeof(TEntity).Name + "Id");
            var value = Expression.Constant(id);
            var equal = Expression.Equal(prop, value);
            var lambda = Expression.Lambda<Func<TEntity, bool>>(equal, item);

            return GetAsNoTrackingQueryable().SingleOrDefault(lambda);
        }

        public int Commit()
        {
            return SaveChanges();
        }

        #region IQueryable

        protected IQueryable<TEntity> GetAsQueryable()
        {
            return Set<TEntity>();
        }

        protected IQueryable<TEntity> GetAsNoTrackingQueryable()
        {
            return Set<TEntity>().AsNoTracking();
        }

        #endregion
    }
}