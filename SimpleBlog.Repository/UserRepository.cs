﻿using SimpleBlog.DomainModel;

namespace SimpleBlog.Repository
{
    public class UserRepository : BaseRepository<ApplicationUser, ApplicationUser>
    {
        public UserRepository(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }
    }
}