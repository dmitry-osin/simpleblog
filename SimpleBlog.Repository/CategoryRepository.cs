﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleBlog.Contracts.Repository;
using SimpleBlog.DomainModel;

namespace SimpleBlog.Repository
{
    public class CategoryRepository : BaseRepository<Category, ApplicationUser>, ICategoryRepository
    {
        public CategoryRepository() : base("DefaultConnection")
        {
        }

        public CategoryRepository(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        public IEnumerable<Category> GetAll()
        {
            return GetAsNoTrackingQueryable().ToArray();
        }

        public Category GetByName(string name)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            return GetAsNoTrackingQueryable().FirstOrDefault(c => c.Name == name);
        }

        public IEnumerable<Category> GetByCount(int count)
        {
            return GetAsNoTrackingQueryable().Take(count).ToArray();
        }

        #region DbSet

        public DbSet<Category> Categories { get; set; }

        #endregion

        #region OnModelCreating

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Mappings(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private static void Mappings(DbModelBuilder modelBuilder)
        {
            modelBuilder.RegisterEntityType(typeof(Category));
            modelBuilder.Entity<Category>().ToTable("Categories");

            modelBuilder.Entity<Category>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Category>()
                .Property(x => x.Name)
                .HasMaxLength(150)
                .IsRequired();

            modelBuilder.Entity<Category>()
                .Property(x => x.Description)
                .HasMaxLength(300)
                .IsOptional();

            modelBuilder.Entity<Category>()
                .Property(x => x.UrlSlug)
                .IsRequired();
        }

        #endregion
    }
}