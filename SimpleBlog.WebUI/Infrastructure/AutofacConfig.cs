﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using NLog;
using SimpleBlog.Contracts.Repository;
using SimpleBlog.Repository;

namespace SimpleBlog.WebUI.Infrastructure
{
    public static class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<PostRepository>()
                .As<IPostRepository>();

            builder.RegisterType<TagRepository>()
                .As<ITagRepository>();

            builder.RegisterType<CategoryRepository>()
                .As<ICategoryRepository>();

            builder.Register(context => LogManager.GetCurrentClassLogger())
                .As<ILogger>();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}