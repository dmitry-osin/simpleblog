﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleBlog.DomainModel;
using SimpleBlog.Repository.Utils;

namespace SimpleBlog.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new DbInitializer.CategoriesDbInitializer());
            Database.SetInitializer(new DbInitializer.TagsDbInitializer());
            Database.SetInitializer(new DbInitializer.PostsDbInitializer());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
